package test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import Pages.CerotidPageObjectsAndMethods;

public class CerotidPageObjectsAndMethodsTest {
	
	public static WebDriver driver = null;
	
	public static void main(String[] args) {
		
		//invoke browser
		invokeBrowser();
		
		//fill form
		fillForm();
		
		//Validate the success message
		successMessage();
		
		//Closing the Website
		
		closing();
	}

	
	private static void closing() {
		// TODO Auto-generated method stub
		
		driver.close();
		driver.quit();
		
	}


	public static void successMessage() {
		String expectedMessage = "Your register is completed. We will contact you shortly!";
		
		String actualResult = driver.findElement(By.xpath("//strong[contains(text(),\"Your register is completed\")]")).getText();
		
		if(expectedMessage.equalsIgnoreCase(actualResult)) {
			System.out.println("Pass");
		}
		else {
			System.out.println("Fail");
			System.out.println("ActualMessage: " + actualResult);
		}
		
	}

	public static void fillForm() {
		//Creating object so that we can use elements from CerotidPageObjectsAndMethods
		CerotidPageObjectsAndMethods cerotidPageObj = new CerotidPageObjectsAndMethods(driver);
		cerotidPageObj.selectCourse("Java");
		cerotidPageObj.sessionDate("Upcoming Session");
		cerotidPageObj.fullName("Testing");
		cerotidPageObj.address("123 east, testing");
		cerotidPageObj.city("Kansas");
		cerotidPageObj.state("TX");
		cerotidPageObj.zip("52167");
		cerotidPageObj.email("testing@gmail.com");
		cerotidPageObj.phone("9123456779");
		cerotidPageObj.visaStatus("OPT");
		cerotidPageObj.media("Social Media");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		cerotidPageObj.relocate("N0");
		cerotidPageObj.education("testing check");
		cerotidPageObj.submitBtn();
		
		
		
	}

	public static void invokeBrowser() {
		//Set the system path
		
		System.setProperty("webdriver.chrome.driver", "./libs/chromedriver");
		
		//Create chromedriver obj
		
		driver = new  ChromeDriver();
		
		//Navigate to cerotid page
		driver.navigate().to("http://cerotid.com/");
		
		//Check if the page is correct.
		
		if(driver.getTitle().contains("Cerotid")) {
			System.out.println("Correct Page");
		}
		else {
			System.out.println("Incorrect Page");
		}
		
		//Maximize the screen
		
		driver.manage().window().maximize();
		
		//Implicit wait time
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
	}

}
