package test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import Pages.cerotidPages;

public class cerotidPagesTest {

	private static WebDriver driver = null;

	public static void main(String[] args) throws InterruptedException {
        //invoking Browser
		invokeBrowser();
		
		//Filling the form
		fillForm();

	}

	public static void fillForm() {
		
		System.out.println("inside fillForm");

		//Utilizing cerotidPage object/Element to select course
		Select selectCourse = new Select(cerotidPages.courses(driver));
		selectCourse.selectByVisibleText("Security");
		
		//Utilizing cerotidPage object/Element to select session Date
		Select selectDate = new Select(cerotidPages.sessionDate(driver));
		selectDate.selectByVisibleText("Upcoming Session");
		
		//Entering name in full name 
		cerotidPages.fullName(driver).sendKeys("Testing");
	}

	public static void invokeBrowser() throws InterruptedException {
		
		System.out.println("inside invokeBrowser");
		//set the system path
		System.setProperty("webdriver.chrome.driver", "./libs/chromedriver");
		driver = new ChromeDriver();
	  

		// navigate to Cerotid page
		driver.navigate().to("http://cerotid.com/");

		// Maximize the screen
		driver.manage().window().maximize();
        System.out.println("driver  "+driver);
		

		Thread.sleep(1000);
		// driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		System.out.println("exit invokeBrowser");

	}

}
