package LinearAutomation;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CerotidWebsiteLineExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./libs/chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.navigate().to("http://cerotid.com/");
		
		driver.manage().window().maximize();
		
		//create a webElement object
		
		WebElement selectCourse = driver.findElement(By.xpath("//select[@id='classType']"));
		
		//Create Select object and pass the element we want to select.
		Select courseName = new Select(selectCourse);
		
		//Create a String variable 
		
		String name = "QA Automation";
		
		//Select the course by visible text
		
		courseName.selectByVisibleText(name);
		
		WebElement radioButton = driver.findElement(By.xpath("(//input[@id='relocate'])[2]"));
		
		radioButton.click();
		
		WebElement selectSessionDate = driver.findElement(By.xpath("//select[@id='sessionType']"));
		
		Select sessionDate = new Select(selectSessionDate);
		
		String date = "Upcoming Session";
		
		sessionDate.selectByVisibleText(date);
		
		WebElement firstName = driver.findElement(By.xpath("//input[@id='name']"));
		
		firstName.sendKeys("Nista Shrestha");
		
		firstName.sendKeys(Keys.RETURN);
		
		driver.findElement(By.xpath("//input[@id='address']")).sendKeys("350 East Vista Ridge Mall dr");
		
		driver.findElement(By.xpath("//input[@id='city']")).sendKeys("Lewisville");
		
		WebElement state = driver.findElement(By.xpath("//select[@id='state']"));
		
		Select stateName = new Select(state);
		
		String name1 = "TX";
		
		stateName.selectByVisibleText(name1);

		driver.findElement(By.xpath("//input[@id='zip']")).sendKeys("75067");

		driver.findElement(By.xpath("//input[@id='email']")).sendKeys("sthanista@gmail.com");
		
		driver.findElement(By.xpath("//input[@id='phone']")).sendKeys("9123456789");
		
		
		
		
		

	}

}
