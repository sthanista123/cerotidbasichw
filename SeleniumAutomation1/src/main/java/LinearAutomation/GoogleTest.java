package LinearAutomation;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class GoogleTest {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./libs/chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.google.com/");
		Thread.sleep(1000);
		
		driver.manage().window().maximize();
		if (driver.getTitle().contains("Google")) {
			System.out.println("Pass - things are working");
			
		}
		else {
			System.out.println("Fail - things are not working properly");
		}
		
		WebElement textBox = driver.findElement(By.xpath("//input[@name='q']"));
		textBox.sendKeys("What is selenium");
		textBox.sendKeys(Keys.RETURN);
		
		if (driver.getTitle().contains("What is selenium - Google Search")) {
			System.out.println("Hurray");
		}
		else {
			System.out.println("oops");
		}
		
		

	}

}
