package AirlinesHw;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FrontierAirlineHw {

	// Class level variable
	static WebDriver driver;

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		// Invoke the Browser
		invokeBrowser();

		// Fill the form

		fillForm();
		
		//Validate
		Validate();
		
		driver.close();
		
		driver.quit();
		
		

	}

	public static void Validate() throws InterruptedException {
		// TODO Auto-generated method stub
		Thread.sleep(1000);
		
		String text1 = "ROUND-TRIP FARES";
		String validateText1 = driver.findElement(By.xpath("(//div[contains(text(),'Round-trip Fares')])[1]")).getText();
		
		if (validateText1.equalsIgnoreCase(text1)) {
			System.out.println("Pass " + text1 + " matches");
		}
		else {
			System.out.println("Fail");
		}
		
//		String text2 = "Dallas/Ft. Worth, TX (DFW) to Los Angeles, CA (LAX)";
//		String validateText2 = driver.findElement(By.xpath("//div[contains(text(),'Dallas/Ft. Worth, TX (DFW) to Los Angeles, CA (LAX))]")).getText();
//		if (validateText2.equalsIgnoreCase(text2)) {
//			System.out.println("Pass");
//		}
//		else {
//			System.out.println("Fail");
//		}
	}

	public static void fillForm() throws InterruptedException {
		// TODO Auto-generated method stub

		try {
			// Close the cookies option
			WebElement closeXbtn = driver.findElement(By.xpath("(//button[@aria-label='Close'])[3]"));
			closeXbtn.click();

			// departure object
			WebElement departure = driver.findElement(By.xpath("//input[@name='kendoDepartFrom_input']"));
			departure.sendKeys("DFW");
			departure.sendKeys(Keys.TAB);
			Thread.sleep(1000);

			// arrival object
			WebElement arrival = driver.findElement(By.xpath("//input[@name='kendoArrivalTo_input']"));
			arrival.sendKeys("LAX");
			// driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
			Thread.sleep(5000);
			arrival.sendKeys(Keys.ENTER);
			Thread.sleep(5000);

			// Calendar icon

			WebElement icon = driver.findElement(By.xpath("//input[@id='departureDate']"));
			icon.click();

			WebElement departureDate = driver.findElement(By.xpath("//a[@id='7-15-2020']"));
			departureDate.click();

			// Store the current window
			String firstWindow = driver.getWindowHandle();

			// Click close when window opens
			for (String windows : driver.getWindowHandles()) {
				driver.switchTo().window(windows);
			}
			driver.close();
			driver.switchTo().window(firstWindow);

			WebElement arrivalDate = driver.findElement(By.xpath("//a[@id='7-25-2020']"));
			arrivalDate.click();

			WebElement passengers = driver.findElement(By.xpath("//input[@id='passengersInput']"));
			passengers.click();

			WebElement passengerAdd = driver.findElement(By.xpath(("(//img[@role='button'])[2]")));
	        passengerAdd.click();

			Thread.sleep(2000);
			passengers.sendKeys(Keys.ENTER);
		

			// Search button
			WebElement searchBtn = driver.findElement(By.xpath("(//img[@class='SearchSVG'])[1]"));
			searchBtn.click();

			
		}
		catch(Exception e){
			e.printStackTrace();
			e.getCause();
			e.getStackTrace();
			
		}
		
	}

	public static void invokeBrowser() {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "./libs/chromedriver");

		// chromeDriver object
		driver = new ChromeDriver();

		// Navigate to FrontierAirline
		driver.get("https://www.flyfrontier.com");

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();

	}

}
