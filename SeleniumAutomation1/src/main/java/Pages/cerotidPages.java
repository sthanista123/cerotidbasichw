package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class cerotidPages {
	//Creating class level variable to store web element value
	private static WebElement element = null;

	//Course Element
	public static WebElement courses(WebDriver driver){
		//Find the elements and return the value
		//System.out.println("deriver "+driver);
		element = driver.findElement(By.xpath("//select[@id='classType']"));
		return element;
		
	}
	
	//Session Element
	public static WebElement sessionDate(WebDriver driver) {
		element = driver.findElement(By.xpath("//select[@id='sessionType']"));
		return element;
		
	}
	
	//Full Name field
	public static WebElement fullName(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@placeholder='Full Name *']"));
		return element;
		
	}
	
	//Address field
	public static WebElement address(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@placeholder='Address *']"));
		return element;
		
	}
	
	//City field
	public static WebElement city(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@placeholder='City *']"));
		return element;
		
	}
	
	//State Element
	public static WebElement state(WebDriver driver) {
		element = driver.findElement(By.xpath("//select[@id='state']"));
		return element;
	}
	
	//Zip field
	public static WebElement zip(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='zip']"));
		return element;
	}
	
	//Email field
	public static WebElement email(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@placeholder='Email *']"));
		return element;
	}
	
	//Phone Field
	
	public static WebElement phone(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@placeholder='Phone *']"));
		return element;
	}
	
	//Visa status Element
	public static WebElement visaStatus(WebDriver driver) {
		element = driver.findElement(By.xpath("//select[@id='visaStatus']"));
		return element;
	}
	
	//media Element
	public static WebElement media(WebDriver driver) {
		element = driver.findElement(By.xpath("//select[@id='mediaSource']"));
		return element;
	}
	
	//relocate -NO Element
	
	public static WebElement relocate(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@value='N0']"));
		return element;
	}
	
	//education Background
	public static WebElement education(WebDriver driver) {
		element = driver.findElement(By.xpath("//textarea[@id='eduDetails']"));
		return element;
	}
	
	//submit Element
	
	public static WebElement submitBtn(WebDriver driver) {
		element = driver.findElement(By.xpath("//button[@id='registerButton']"));
		return element;
	}
	
	

}
