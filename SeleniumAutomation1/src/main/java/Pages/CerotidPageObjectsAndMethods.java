package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class CerotidPageObjectsAndMethods {
	
	//Create a empty WebDriver
	WebDriver driver = null;
	
	//Locating Elements using BY Object
	By selectCourse = By.xpath("//select[@id='classType']");
	By sessionDate = By.xpath("//select[@id='sessionType']");
	By fullName = By.xpath("//input[@placeholder='Full Name *']");
	By address = By.xpath("//input[@placeholder='Address *']");
	By city = By.xpath("//input[@placeholder='City *']");
	By state = By.xpath("//select[@id='state']");
	By zip = By.xpath("//input[@id='zip']");
	By email = By.xpath("//input[@placeholder='Email *']");
	By phone = By.xpath("//input[@placeholder='Phone *']");
	By visaStatus = By.xpath("//select[@id='visaStatus']");
	By media = By.xpath("//select[@id='mediaSource']");
	By relocate = By.xpath("//input[@value='N0']");
	By education = By.xpath("//textarea[@id='eduDetails']");
	By submitBtn = By.xpath("//button[@id='registerButton']");
	
	//Create a constructor so we can utilize WebDriver
	public CerotidPageObjectsAndMethods(WebDriver driver) {
		this.driver = driver;
		
	}
	
	public void selectCourse(String course) {
		//Creating new WebElement object and passing By(selectCourse) as argument
		WebElement element = driver.findElement(selectCourse);
		
		//Creating select object
		Select chooseCourse = new Select(element);
		chooseCourse.selectByVisibleText(course);
			
	}
	
	public void sessionDate(String sessionType) {
		WebElement element = driver.findElement(sessionDate);
		
		Select type = new Select(element);
		
		type.selectByVisibleText(sessionType);
	}
	
	public void fullName(String name) {
		driver.findElement(fullName).sendKeys(name);
		
	}
	
	public void address(String address1) {
		driver.findElement(address).sendKeys(address1);
	}
	
	 public void city(String city1) {
		 driver.findElement(city).sendKeys(city1);
	 }
	 
	 public void state(String state1) {
		 WebElement element = driver.findElement(state);
		 Select choseState = new Select(element);
		 choseState.selectByVisibleText(state1);
	 }
	 
	 public void zip(String zip1) {
		 driver.findElement(zip).sendKeys(zip1);
	 }
	
	 public void email(String email1) {
		 driver.findElement(email).sendKeys(email1);
	 }
	 
	 public void phone(String phone1) {
		 driver.findElement(phone).sendKeys(phone1);
	 }

	 public void visaStatus(String visa) {
		WebElement element =  driver.findElement(visaStatus);
		Select selectVisa = new Select(element);
		selectVisa.selectByVisibleText(visa);
	 }
	 
	 public void media(String media1) {
		 WebElement element = driver.findElement(media);
		 Select selectMedia = new Select(element);
		 selectMedia.selectByVisibleText(media1);
	 }
	 
	 public void relocate(String relocate1) {
		 driver.findElement(relocate).click();
	 }
	 public void education(String educationBackground) {
		 driver.findElement(education).sendKeys(educationBackground);
		 
	 }
	 
	 public void submitBtn() {
		 driver.findElement(submitBtn).click();
		 
	 }
	 

}
