import java.util.Scanner;

public class homework {

	public static void main(String[] args) {
		double subjects[] = new double[5];
		double sum = 0.0;
		double average = 0.0;
		Scanner co = new Scanner(System.in);

		for (int i = 0; i < subjects.length; i++) {
			System.out.println("Please Enter Course " + (i + 1) + " Grade");
			subjects[i] = co.nextDouble();
			sum = sum + subjects[i];

		}

		average = sum / 5;
		GradeCalculator(average);

	}

	static void GradeCalculator(double score) {
		System.out.print("Average Grade: ");
		if (score >= 90.00 && score <= 100.00) {
			System.out.println("Grade A");
		} else if (score >= 80.00 && score <= 89.00) {
			System.out.println("Grade B");
		} else if (score >= 70.00 && score <= 79.00) {
			System.out.println("Grade C");

		} else if (score >= 60.00 && score <= 69.00) {
			System.out.println("Grade D");
		} else
			System.out.println("Grade F");

	}

}
